from flask import Flask, jsonify, request
from flask_cors import CORS
from apscheduler.schedulers.background import BackgroundScheduler

app = Flask(__name__)
persoList = [{
    "id": 0,
    "nom": "Pacman",
    "dps" : 1,
    "image" : "https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Pacman.svg/1024px-Pacman.svg.png",
    "prix" : 10
},{
    "id": 1,
    "nom": "Mario",
    "dps" : 5,
    "image" : "https://i.jeuxactus.com/datas/jeux/n/e/new-super-mario-bros-2/xl/new-super-mario-bros-2-a-50228d4fd0c4c.jpg",
    "prix" : 50
},{
    "id": 2,
    "nom": "Sonic",
    "dps" : 10,
    "image" : "http://i58.servimg.com/u/f58/13/49/75/95/main23.png",
    "prix" : 250
},{
    "id": 3,
    "nom": "Kirby",
    "dps" : 25,
    "image" : "https://i.jeuxactus.com/datas/jeux/k/i/kirby-triple-deluxe/xl/kirby-triple-deluxe-artwo-5363b01327380.jpg",
    "prix" : 500
},{
    "id": 4,
    "nom": "Crash",
    "dps" : 40,
    "image" : "https://www.stickpng.com/assets/images/5ba0d37044d37f02b75178f0.png",
    "prix" : 1250
},{
    "id": 5,
    "nom": "Rayman",
    "dps" : 75,
    "image" : "https://raymanpc.com/wiki/images/thumb/f/fd/RaymanArtworkRender.png/320px-RaymanArtworkRender.png",
    "prix" : 40*50
},{
    "id": 6,
    "nom": "Link",
    "dps" : 100,
    "image" : "https://www.palaiszelda.com/images/z13/perso/link2.jpg",
    "prix" : 75*50
},
]

banque= 0
inventaire=[0,0,0,0,0,0,0]

def update():
    global banque
    for k in range(0,7):
        banque+=persoList[k]["dps"]*inventaire[k]

sched=BackgroundScheduler(daemon=True)
sched.add_job(update,'interval',seconds=1)
sched.start()

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
@app.route('/api/persoList',methods=['GET'])
def getPersoList():
    return jsonify(persoList)

@app.route('/api/achat',methods=['GET'])
def achatPossible():
    id = request.args.get('id')
    id = int(id)
    global banque
    global inventaire
    prix=persoList[id]["prix"]
    res = banque-prix
    if(res>=0):
        banque = res
        inventaire[id] += 1
    return jsonify(inventaire[id])

@app.route('/api/vente',methods=['GET'])
def vente():
    id = request.args.get('id')
    id = int(id)
    global banque
    global inventaire
    if (inventaire[id] > 0):
        banque+=persoList[id]["prix"]*0.5
        inventaire[id] -= 1
    return jsonify(inventaire[id])

@app.route('/api/getpixel',methods=['GET'])
def getpixel():
    global banque
    return jsonify(banque)

@app.route('/api/clicker',methods=['GET'])
def clicker():
    global banque
    banque += 1
    return jsonify(banque)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
    update(1)
