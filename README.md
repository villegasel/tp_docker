# Clicker Docker
## Description

The application is an idle game. It consists to recolt pixels by attacking an enemy and unlock new partners to recolt more pixels.

### Front
The application is developed with the framework VueJs.
The page is displayed by the docker image Nginx.

### Back

The Back is developed with Python using Flask.
##  Launch

In a terminal in the main folder type the following command:

    sudo docker-compose up

You can access to the game via typing in a browser this address:

[http://localhost:80](http://localhost:80)
