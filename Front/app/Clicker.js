Vue.component('perso-detail', {
	props: ['perso'],
  data: function () {
    return {
      nbAchetes:0,
    }
  },
  template: `
	<div class="card">
    <div class="image" style="background-color:rgb(255,255,255);">
    	<img :src="perso.image" class="card-img-top"></img>
    </div>
    <div class="content ">
        <div class="header card-header">
				{{perso.nom}}
        </div>
        <div class="meta card-body">
        	<div class="card-text" style="text-align: center;">
        		<div><p class="ui teal label"  >DPS : {{perso.dps}}</p></div>
						<br>
						<div><p class="ui teal label"  >Inventaire : {{this.nbAchetes}}</p></div>
        </div>
        <br>
        <div class="flex"><button class="btn btn-success" v-on:click="achat(perso.id)">Achat</button><button v-on:click="vente" class="btn btn-danger">Vente</button></div>
        </div>
    </div>
    <div class="extra content card-footer" style="height: 100%;">
	    	{{perso.prix}} pixels
	    </div>
</div>`,
  methods : {
  	achat : function(id){
				axios.get('http://localhost:5000/api/achat',{
    				params: {
      				id: id
    				}
					})
	        .then(response =>
						this.nbAchetes=response.data
					)
	        .catch(error =>
						this.nbAchetes = this.nbAchetes
					);
	  	},
			vente : function(){
				axios.get('http://localhost:5000/api/vente',{
    				params: {
      				id: id
    				}
					})
				.then(response =>
					this.nbAchetes=response.data
				)
				.catch(
					this.nbAchetes = this.nbAchetes
					);
	  	},
  }
});

Vue.component('clicker', {
	data: function () {
    return {
      bank:0,
    }
  },
  template:
		`
		<div class="card">
		<div class="header card-header">
		Banque : {{this.bank}}
		</div>
    	<div class="image" style="background-color:rgb(255,255,255);">
    		<img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Goomba.PNG/220px-Goomba.PNG"></img>
    	</div>
    	<div class="content ">
        <div class="meta card-body">
        	<button v-on:click="tap" class="btn btn-danger">Kill</button>
        </div>
    	</div>
		</div>`,
		created() {
			this.interval = setInterval(() => this.affichage(), 1000);
		},
  methods : {
		tap : function(){
				axios.get('http://localhost:5000/api/clicker')
	        .then(response =>
						this.bank = response.data
					)
	        .catch(error =>
						this.bank = this.bank
					);
	  	},
		affichage: function() {
					axios.get('http://localhost:5000/api/getpixel')
					.then(response =>
						this.bank = response.data
					)
					.catch(error =>
						this.bank = this.bank
					);
			}
  }
});

var app = new Vue({
    el: '#app',
    data: {
        persoList: []
    },
    mounted() {
    axios.get('http://localhost:5000/api/persoList')
        .then(response => this.persoList=response.data)
        .catch(error => this.persoList = [])
  	}
})
